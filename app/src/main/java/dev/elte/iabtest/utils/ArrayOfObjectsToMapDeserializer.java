package dev.elte.iabtest.utils;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ArrayOfObjectsToMapDeserializer
        implements JsonDeserializer<Map<String, String>> {

    private static String TAG = ArrayOfObjectsToMapDeserializer.class.getSimpleName();

    public Map<String, String> deserialize(JsonElement json, Type typeOfT,
                                           JsonDeserializationContext context) throws JsonParseException {
        Map<String, String> result = new HashMap<String, String>();

        JsonObject object = json.getAsJsonObject();

        for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
            String key = entry.getKey();

            String value = "";
            if (!entry.getValue().isJsonNull()){
                value = entry.getValue().getAsString();
            }

            Log.e(TAG, "value "+value);

            result.put(key, value);
        }

        return result;
    }
}
