package dev.elte.iabtest.utils;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GlobalHelper {

    private static String TAG = GlobalHelper.class.getSimpleName();

    public static void showActivity(Activity from, Class<?> to, boolean finish) {
        Intent intent = new Intent(from, to);
        from.startActivity(intent);
        if (finish) {
            from.finish();
        }
    }

    public static List<View> findViewWithTagRecursively(ViewGroup root){
        List<View> allViews = new ArrayList<View>();

        final int childCount = root.getChildCount();
        for(int i=0; i<childCount; i++){
            final View childView = root.getChildAt(i);

            if(childView instanceof ViewGroup){
                allViews.addAll(findViewWithTagRecursively((ViewGroup)childView));
            }
            else{
                final Object tagView = childView.getTag();
                if(tagView != null)
                    allViews.add(childView);
            }
        }

        return allViews;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        //dd/MM/yyyy hh:mm:ss.SSS
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }

        Log.e(TAG, "index "+index);

        return index;
    }

    public static long timeMilis(String datetime, String format){
        SimpleDateFormat f = new SimpleDateFormat(format);
        try {
            Date d = f.parse(datetime);
            long milliseconds = d.getTime();
            return milliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}
