package dev.elte.iabtest.utils;

import dev.elte.iabtest.BuildConfig;

public class GlobalVars {
    public static String API_URL = BuildConfig.SERVER_URL;
}
