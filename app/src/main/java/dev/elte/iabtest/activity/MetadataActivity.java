package dev.elte.iabtest.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import dev.elte.iabtest.R;
import dev.elte.iabtest.model.Metadata;
import dev.elte.iabtest.model.MetadataPayload;

import static dev.elte.iabtest.utils.GlobalHelper.getDate;
import static dev.elte.iabtest.utils.GlobalHelper.getIndex;
import static dev.elte.iabtest.utils.GlobalHelper.showActivity;
import static dev.elte.iabtest.utils.GlobalHelper.timeMilis;
import static dev.elte.iabtest.utils.GlobalVars.API_URL;

public class MetadataActivity extends AppCompatActivity {

    private static String TAG = MetadataActivity.class.getSimpleName();

    private Gson gson;

    private LinearLayout ll;
    private Button btnInsert;
    private Button btnList;

    private List<MetadataPayload> payloadList;
    private JSONObject dataObj;

    private Map<String, String> hashMap;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metadata);

        intent = getIntent();

        gson = new Gson();
        payloadList= new ArrayList<>();
        dataObj = new JSONObject();

        btnInsert = findViewById(R.id.btnInsert);
        btnList = findViewById(R.id.btnList);
        ll = (LinearLayout)findViewById(R.id.rootLayout);

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crudData(false);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (hashMap!=null){
                    crudData(true);
                }else{
                    showActivity(MetadataActivity.this, FieldActivity.class, false);
                }
            }
        });

        if (intent.hasExtra("map")){
            hashMap = (Map<String, String>)intent.getSerializableExtra("map");
            btnList.setText(getString(R.string.delete));
            btnInsert.setText(getString(R.string.update));
            getMetadata(hashMap);
        }else{
            getMetadata(hashMap);
        }

    }

    private void getMetadata(final Map<String, String> hashMap){

        JSONObject mainObj = new JSONObject();
        JSONObject pObj = new JSONObject();
        try {
            mainObj.put("a", "metadata");
            mainObj.put("p", pObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(API_URL)
                .addJSONObjectBody(mainObj)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Metadata metadata = gson.fromJson(response.toString(), Metadata.class);

                        if (metadata.getErrorCode()==0){
                            List<MetadataPayload> metadataPayloadsList = metadata.getPayload();

                            if (metadataPayloadsList!=null && !metadataPayloadsList.isEmpty()){
                                for (MetadataPayload metadataPayload:metadataPayloadsList){
                                    addForm(metadataPayload.getInput(), metadataPayload, hashMap);
                                }
                            }

                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.e(TAG, "ANError "+error.getErrorDetail());
                    }
                });
    }

    private void addForm(String input, MetadataPayload metadataPayload, Map<String, String> hashMap){
        final String field = metadataPayload.getField();
        String type = metadataPayload.getType();
        String maxLength = metadataPayload.getMaxLength();


        int i = 0;

        if (input.equals("text")){
            i++;

            MetadataPayload metadataPayload1 = new MetadataPayload(field, type, maxLength, input, metadataPayload.getAccepted());

            payloadList.add(metadataPayload1);

            // add title
            TextView tv=new TextView(this);
            tv.setText(field+"-"+type+"-"+maxLength+"-"+input);
            ll.addView(tv);

            String uniqueId = "";
            String text = "";

            if (hashMap!=null){
                uniqueId = getHashmapValue(hashMap, field);
                text = getHashmapValue(hashMap, field);
            } else{
                uniqueId = UUID.randomUUID().toString();
            }

            // add edittext
            EditText et = new EditText(this);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            et.setLayoutParams(p);
            et.setTag(field);
            et.setId(i);

            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(Integer.parseInt(maxLength));

            et.setFilters(fArray);

            if (metadataPayload.getField().equals("crud_id")){
                et.setText(uniqueId);
                try {
                    dataObj.put(field, et.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                et.setText(text);
                et.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            dataObj.put(field, s.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }
            ll.addView(et);



        }

        if (input.equals("textarea")){

            i++;

            MetadataPayload metadataPayload1 = new MetadataPayload(field, type, maxLength, input, metadataPayload.getAccepted());

            payloadList.add(metadataPayload1);

            // add title
            TextView tv=new TextView(this);
            tv.setText(field+"-"+type+"-"+maxLength+"-"+input);
            ll.addView(tv);

            String text = "";

            if (hashMap!=null){
                text = getHashmapValue(hashMap, field);
            }

            // add edittext
            final EditText et = new EditText(this);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            et.setLayoutParams(p);
            et.setText(text);
            et.setTag(field);
            et.setSingleLine(false);
            et.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            et.setLines(2);
            et.setMaxLines(10);
            et.setId(i);

            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        dataObj.put(field, s.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            ll.addView(et);

        }


        if (input.equals("combobox")){

            i++;

            MetadataPayload metadataPayload1 = new MetadataPayload(field, type, maxLength, input, metadataPayload.getAccepted());

            payloadList.add(metadataPayload1);

            // add title
            TextView tv=new TextView(this);
            tv.setText(field+"-"+type+"-"+maxLength+"-"+input);
            ll.addView(tv);

            final List<String> myList = new ArrayList<String>(Arrays.asList(metadataPayload.getAccepted().split("\\|")));

            Spinner spinner = new Spinner(this);
            spinner.setTag(field);
            spinner.setId(i);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, myList);
            spinner.setAdapter(spinnerArrayAdapter);

            if (hashMap!=null){
                String text = getHashmapValue(hashMap, field);

                int spinIdx = getIndex(spinner, text);

                spinner.setSelection(spinIdx);
            }

            ll.addView(spinner);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        dataObj.put(field, myList.get(position).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (input.equals("checkbox")){

            MetadataPayload metadataPayload1 = new MetadataPayload(field, type, maxLength, input, metadataPayload.getAccepted());

            payloadList.add(metadataPayload1);

            // add title
            TextView tv=new TextView(this);
            tv.setText(field+"-"+type+"-"+maxLength+"-"+input);
            ll.addView(tv);

            List<String> myList = new ArrayList<String>(Arrays.asList(metadataPayload.getAccepted().split("\\|")));

            final RadioButton[] rb = new RadioButton[5];
            RadioGroup rg = new RadioGroup(this); //create the RadioGroup
            rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
            String text = "";

            for(int idx=0; idx<myList.size(); idx++){
                i++;
                rb[idx]  = new RadioButton(this);
                rb[idx].setText(myList.get(idx));
                rb[idx].setId(i);

                if (hashMap!=null){
                    text = getHashmapValue(hashMap, field);

                    if (text.equals(myList.get(idx))){
                        rb[idx].setChecked(true);
                    }
                }

                rg.addView(rb[idx]);
            }

            if (hashMap==null){
                rb[0].setChecked(true);
            }

            try {
                dataObj.put(field, rb[0].getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    RadioButton selectedRadioButton = (RadioButton) findViewById(checkedId);
                    String selectedRadioButtonText = selectedRadioButton.getText().toString();

                    try {
                        dataObj.put(field, selectedRadioButtonText);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            ll.addView(rg);//you add the whole RadioGroup to the layout
        }

        if (input.equals("datepicker")){

            i++;

            MetadataPayload metadataPayload1 = new MetadataPayload(field, type, maxLength, input, metadataPayload.getAccepted());

            payloadList.add(metadataPayload1);

            // add title
            TextView tv=new TextView(this);
            tv.setText(field+"-"+type+"-"+maxLength+"-"+input);
            ll.addView(tv);

            CalendarView calendarView = new CalendarView(this);
            calendarView.setTag(field);
            calendarView.setId(i);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(90, 40, 80, 40);
            calendarView.setLayoutParams(layoutParams);

            String text = "";

            if (hashMap!=null){
                text = getHashmapValue(hashMap, field);

                long convertedDate = timeMilis(text, "yyyy-MM-dd'T'HH:mm:ss.SSS");

                calendarView.setDate (convertedDate, true, true);
            }else{

            }

            final String[] selectedDate = {getDate(calendarView.getDate(), "yyyy-MM-dd hh:mm:ss") + ""};

            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                    selectedDate[0] = getDate(view.getDate(), "yyyy-MM-dd hh:mm:ss")+"";
                }
            });

            try {
                dataObj.put(field,  selectedDate[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ll.addView(calendarView);
        }
    }

    private void crudData(boolean isDelete){
        JSONObject mainObj = new JSONObject();
        try {

            if (isDelete==true){
                mainObj.put("a", "delete");
            }else{
                if (intent.hasExtra("map")){
                    mainObj.put("a", "update");
                }else {
                    mainObj.put("a", "insert");
                }
            }



            mainObj.put("p", dataObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(API_URL)
                .addJSONObjectBody(mainObj)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Metadata metadata = gson.fromJson(response.toString(), Metadata.class);
                        Toast.makeText(getApplicationContext(), metadata.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        showActivity(MetadataActivity.this, FieldActivity.class, false);
//
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.e(TAG, "ANError "+error.getErrorDetail());
                    }
                });
    }

    private String getHashmapValue(Map<String, String> hashMap, String field){

        String s = "";

        Iterator it = hashMap.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();

            if (pair.getKey().equals(field)){
                s = pair.getValue().toString();
            }
        }

        return s;
    }
}
