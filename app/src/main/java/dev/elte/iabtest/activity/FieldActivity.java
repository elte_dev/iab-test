package dev.elte.iabtest.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.elte.iabtest.R;
import dev.elte.iabtest.adapter.FieldAdapter;
import dev.elte.iabtest.model.Field;
import dev.elte.iabtest.utils.ArrayOfObjectsToMapDeserializer;

import static dev.elte.iabtest.utils.GlobalVars.API_URL;

public class FieldActivity extends AppCompatActivity {
    private static String TAG = FieldActivity.class.getSimpleName();
    private  FieldAdapter fieldAdapter;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field);

        rv = findViewById(R.id.rv);

        fieldAdapter = new FieldAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                linearLayoutManager.getOrientation());
        rv.addItemDecoration(dividerItemDecoration);

        rv.setAdapter(fieldAdapter);

        getField();
    }

    private void getField(){

        JSONObject mainObj = new JSONObject();
        JSONObject pObj = new JSONObject();
        try {
            mainObj.put("a", "list");
            mainObj.put("p", pObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(
                new TypeToken<Map<String, String>>() {}.getType(),
                new ArrayOfObjectsToMapDeserializer());
        final Gson gson = builder.create();

        AndroidNetworking.post(API_URL)
                .addJSONObjectBody(mainObj)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Field field = gson.fromJson(response.toString(), Field.class);

                        List<Map<String, String>> payloadMapList = field.getPayload();

                        fieldAdapter.addAll(payloadMapList);

//                        Map<String, String> map = new HashMap<>();
//                        map.put("First", field);

//                        Map<String, String> myMap ;
//
//                        for (int i=0;i<field.getPayload().size();i++){
//                            Log.e(TAG, "field "+field.getPayload().get(i));
//                        }



//                        for (Map.Entry<String, String> entry : map.entrySet()) {
//                            System.out.println(entry.getKey() + " - " + entry.getValue());
//                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.e(TAG, "ANError "+error.getErrorDetail());
                    }
                });
    }
}
