package dev.elte.iabtest.model;

import java.util.List;
import java.util.Map;

public class Field {

    private int errorCode;
    private String errorMessage;
    List<Map<String, String>> payload;

    public Field() {
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<Map<String, String>> getPayload() {
        return payload;
    }

    public void setPayload(List<Map<String, String>> payload) {
        this.payload = payload;
    }

}