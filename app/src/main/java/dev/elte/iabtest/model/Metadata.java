package dev.elte.iabtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Metadata implements Parcelable
{

    private int errorCode;
    private String errorMessage;
    private List<MetadataPayload> payload = new ArrayList<>();
    public final static Parcelable.Creator<Metadata> CREATOR = new Creator<Metadata>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Metadata createFromParcel(Parcel in) {
            return new Metadata(in);
        }

        public Metadata[] newArray(int size) {
            return (new Metadata[size]);
        }

    }
            ;

    protected Metadata(Parcel in) {
        this.errorCode = ((int) in.readValue((int.class.getClassLoader())));
        this.errorMessage = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.payload, (MetadataPayload.class.getClassLoader()));
    }

    public Metadata() {
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<MetadataPayload> getPayload() {
        return payload;
    }

    public void setPayload(List<MetadataPayload> payload) {
        this.payload = payload;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(errorCode);
        dest.writeValue(errorMessage);
        dest.writeList(payload);
    }

    public int describeContents() {
        return 0;
    }

}
