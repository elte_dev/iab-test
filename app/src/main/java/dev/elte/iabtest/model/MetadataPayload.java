package dev.elte.iabtest.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MetadataPayload implements Parcelable
{

    private String field;
    private String type;
    private String maxLength;
    private String input;
    private String accepted;

    public MetadataPayload(String field, String type, String maxLength, String input, String accepted) {
        this.field = field;
        this.type = type;
        this.maxLength = maxLength;
        this.input = input;
        this.accepted = accepted;
    }

    public final static Parcelable.Creator<MetadataPayload> CREATOR = new Creator<MetadataPayload>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MetadataPayload createFromParcel(Parcel in) {
            return new MetadataPayload(in);
        }

        public MetadataPayload[] newArray(int size) {
            return (new MetadataPayload[size]);
        }

    }
            ;

    protected MetadataPayload(Parcel in) {
        this.field = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.maxLength = ((String) in.readValue((String.class.getClassLoader())));
        this.input = ((String) in.readValue((String.class.getClassLoader())));
        this.accepted = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MetadataPayload() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(field);
        dest.writeValue(type);
        dest.writeValue(maxLength);
        dest.writeValue(input);
        dest.writeValue(accepted);
    }

    public int describeContents() {
        return 0;
    }

}