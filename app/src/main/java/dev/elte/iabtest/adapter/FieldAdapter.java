package dev.elte.iabtest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dev.elte.iabtest.R;
import dev.elte.iabtest.activity.FieldActivity;
import dev.elte.iabtest.activity.MetadataActivity;
import dev.elte.iabtest.model.Metadata;

import static dev.elte.iabtest.utils.GlobalHelper.showActivity;

public class FieldAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static String TAG = FieldAdapter.class.getSimpleName();
    List<Map<String, String>> list;
    private Context context;


    public FieldAdapter(Context context) {
        this.context = context;
        //this.mCallback = (PaginationAdapterCallback) context;
        list = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View viewItem = inflater.inflate(R.layout.item_field, parent, false);

        RecyclerView.ViewHolder viewHolder = new ViewHolder(viewItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        //final User result = list.get(position);

        final Map<String, String> map = list.get(position);

        final ViewHolder viewHolder = (ViewHolder) holder;

        //Log.e(TAG, "abc "+  list.get(position).values());

        Iterator it = map.entrySet().iterator();

        String abc = "";

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            abc += pair.getKey() + " = " + pair.getValue()+"\n";
            viewHolder.tvItem.setText(abc);
            //it.remove(); // avoids a ConcurrentModificationException
        }

        viewHolder.tvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e(TAG, "abc "+  list.get(position).values());
                Intent intent = new Intent(context, MetadataActivity.class);
                intent.putExtra("map", (Serializable) map);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    public void add(Map<String, String> r) {
        list.add(r);
        notifyItemInserted(list.size() - 1);
    }

    public void addAll(List<Map<String, String>> moveResults) {
        for (Map<String, String> result : moveResults) {
            add(result);
        }
    }


    /**
     * Main list's content ViewHolder
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItem;


        public ViewHolder(View itemView) {
            super(itemView);
            tvItem = itemView.findViewById(R.id.tvItem);
        }
    }

}